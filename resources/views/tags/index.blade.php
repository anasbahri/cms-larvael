@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-end mb-2">
    <a href="{{ route('tags.create') }}" class="btn btn-success float-right">Add tag</a>
</div>
<div class="card">
    <div class="card-header">
        Tags
    </div>
    <div class="card-body">
        @if ($tags->count()>0)
        <table class="table">
            <thead>
                <th>Names</th>
                <th></th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($tags as $tag)
                <tr>
                    <td>
                        {{ $tag -> name}}
                    </td>
                    <td>
                        <a href="{{ route('tags.edit', $tag->id)}}" class="btn btn-info btn-sm">Edit</a>
                    </td>
                    <td>
                        <a href="{{ route('tags.destroy', $tag->id)}}" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h3 class="text-center">No Tags Yet</h3>
        @endif
    </div>
</div>

@endsection

