@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        {{ isset($tag) ? 'Edit Tag' : 'Create Tag'}}
    </div>
    <div class="card-body">
        @include('partials.errors')
        <form method="POST" action="{{  isset($tag) ? route('tags.update', $tag -> id) : route('tags.store')  }}">
            @csrf
            @if (  isset($tag))
            @method('PUT')
            @endif
            <div class="form-group">
                <label for="name">Name</label>
            <input id="name" class="form-control" value="{{ isset($tag) ? $tag -> name : '' }}" type="text" name="name">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"> {{ isset($tag) ? 'update Tag' : 'Add Tag'}}</button>
            </div>
        </form>
    </div>
</div>

@endsection
