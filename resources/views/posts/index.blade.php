@extends('layouts.app')


@section('content')
<div class="d-flex justify-content-end mb-2">
    <a href="{{ route('posts.create') }}" class="btn btn-success float-right">Add Post</a>
</div>
<div class="card">
    <div class="card-header">
        Posts
    </div>
    <div class="card-body">
        @if ($posts->count()>0)
        <table class="table">
            <thead>
                <th>Image</th>
                <th>title</th>
                <th>Category</th>
                <th></th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                    <td>
                        <img src="http://127.0.0.1:8000/storage/{{ $post -> image }}" width="100px" height="100px"
                            alt="">
                    </td>
                    <td>
                        {{ $post -> title}}
                    </td>
                    <td>
                        {{ $post -> category->name}}
                    </td>
                    @if (!$post->trashed() )
                    <td>
                        <a href="{{ route('posts.edit', $post->id)}}" class="btn btn-info btn-sm">Edit</a>
                    </td>
                    @else
                    <td>
                        <form method="POST" action="{{ route('restore-posts', $post->id)}}">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-info btn-sm">
                                Restore
                            </button>
                        </form>
                    </td>

                    @endif
                    <td>
                        <form method="POST" action="{{ route('posts.destroy', $post->id)}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                {{ $post->trashed() ? 'Delete' : 'Trash' }}
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h3 class="text-center">No Posts Yet</h3>
        @endif
    </div>
    @endsection

</div>
