@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        {{ isset($post) ? 'Edit Post' : 'Create post'}}
    </div>
    <div class="card-body">
        @include('partials.errors')
        <form method="POST" enctype="multipart/form-data"
            action="{{  isset($post) ? route('posts.update', $post -> id) : route('posts.store')  }}">
            @csrf
            @if ( isset($post))
            @method('PUT')
            @endif
            <div class="form-group">
                <label for="title">Title</label>
                <input id="title" class="form-control" value="{{ isset($post) ? $post -> title : '' }}" type="text"
                    name="title">
            </div>
            <div class="form-group">
                <label for="content">Description</label>
                <textarea name="description" id="description" class="form-control" cols="30"
                    rows="5">{{ isset($post) ? $post -> description : '' }}</textarea>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <input id="content" type="hidden" name="content" value="{{ isset($post) ? $post -> content : '' }}">
                <trix-editor input="content"></trix-editor>
            </div>
            <div class="form-group">
                <label for="published_at">Published At</label>
                <input id="published_at" class="form-control" value="{{ isset($post) ? $post -> published_at : '' }}"  type="text" name="published_at">
            </div>
            @if (isset($post))
                <div class="form-group">
                    <img src="http://127.0.0.1:8000/storage/{{ $post -> image }}" style="width: 100%" alt="">
                </div>
            @endif
            <div class="form-group">
                <label for="image">Image</label>
                <input id="image" class="form-control" type="file" name="image">
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="form-control">
                    @foreach ($categories as $category)
                <option value="{{ $category -> id}}"
                    @if (isset($post))
                    @if ( $category -> id == $post->category_id)
                    selected
                    @endif
                    @endif>{{ $category -> name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"> {{ isset($post) ? 'update post' : 'Add post'}}</button>
            </div>
        </form>
    </div>
</div>

@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
        flatpickr('#published_at',{
            enableTime : true
        });
</script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
