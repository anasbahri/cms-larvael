@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        {{ isset($category) ? 'Edit Category' : 'Create Category'}}
    </div>
    <div class="card-body">
        @include('partials.errors')
        <form method="POST" action="{{  isset($category) ? route('categories.update', $category -> id) : route('categories.store')  }}">
            @csrf
            @if (  isset($category))
            @method('PUT')
            @endif
            <div class="form-group">
                <label for="name">Name</label>
            <input id="name" class="form-control" value="{{ isset($category) ? $category -> name : '' }}" type="text" name="name">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"> {{ isset($category) ? 'update Category' : 'Add Categories'}}</button>
            </div>
        </form>
    </div>
</div>

@endsection
