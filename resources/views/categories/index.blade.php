@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-end mb-2">
    <a href="{{ route('categories.create') }}" class="btn btn-success float-right">Add Categorie</a>
</div>
<div class="card">
    <div class="card-header">
        Categories
    </div>
    <div class="card-body">
        @if ($categories->count()>0)
        <table class="table">
            <thead>
                <th>Names</th>
                <th>Posts Count</th>
                <th></th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td>
                        {{ $category -> name}}
                    </td>
                    <td>
                        {{$category ->posts->count()}}
                    </td>
                    <td>
                        <a href="{{ route('categories.edit', $category->id)}}" class="btn btn-info btn-sm">Edit</a>
                    </td>
                    <td>
                        <a href="{{ route('categories.destroy', $category->id)}}" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h3 class="text-center">No Categories Yet</h3>
        @endif
    </div>
</div>

@endsection

